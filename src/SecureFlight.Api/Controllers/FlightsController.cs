﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController : SecureFlightBaseController
{
    private readonly IFlightService _flightService;
    private readonly IMapper _mapper;
    public FlightsController(IFlightService flightService,
                             IMapper mapper)
        : base(mapper)
    {
        _flightService = flightService;
        _mapper = mapper;
       
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await _flightService.GetAllAsync();
        return GetResult<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }


    [HttpPost("AddPassengerToFlight")]
    [ProducesResponseType(typeof(PassengerToFlighResponseDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> AddPassengerToFlight(PassengerToFlighReequestDataTransferObject request)
    {
        var result = await _flightService.AddPassengerToFlight(request.PassengerId, request.FlightId);
        return GetResult<PassengerFlight, PassengerToFlighResponseDataTransferObject>(result);
      
    }







    
}