﻿using SecureFlight.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces;

public interface IFlightService : IService<Flight>
{
    Task<OperationResult<PassengerFlight>> AddPassengerToFlight(string passengerId, long flightId);
}