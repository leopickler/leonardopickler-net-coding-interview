﻿namespace SecureFlight.Api.Models
{
    public class PassengerToFlighResponseDataTransferObject
    {
        public string PassengerId { get; set; }       

        public string PassengerName { get; set; }

        public string PassengerEmail { get; set; }

        public long FlightId { get; set; }

        public string Code { get; set; }

        public string OriginAirport { get; set; }

        public string DestinationAirport { get; set; }
    }
}
