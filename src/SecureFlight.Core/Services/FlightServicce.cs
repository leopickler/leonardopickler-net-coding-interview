﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFlight.Core.Services
{
    public class FlightServicce : BaseService<Flight>, IFlightService
    {
        private readonly IRepository<Flight> _flightRepository;
        private readonly IRepository<PassengerFlight> _passengerFlightRepository;
        private readonly IService<Passenger> _passengertService;
        private readonly IService<Flight> _flightService;

        public FlightServicce(IService<Flight> flightService,
                              IService<Passenger> passengerService,
                              IRepository<PassengerFlight> passengerFlightRepository,
                              IRepository<Flight> flightRepository)
            : base(flightRepository)
        {
            _passengertService = passengerService;
            _passengerFlightRepository = passengerFlightRepository;
            _flightRepository = flightRepository;
            _flightService = flightService;
        }

        public async Task<OperationResult<PassengerFlight>> AddPassengerToFlight(string passengerId, long flightId)
        {
            var flights = await _flightService.FilterAsync(f => f.Id == flightId);
            var passanger = await _passengertService.FilterAsync(f => f.Id == passengerId);
           
            if (!flights.Succeeded)
                return new OperationResult<PassengerFlight>(flights.Error);

            if (!passanger.Succeeded)
                return new OperationResult<PassengerFlight>(passanger.Error);

            if (flights.Result.Count == 1 && passanger.Result.Count == 1)
            {
                var passengerFlight = new PassengerFlight()
                {
                    FlightId = flightId,
                    PassengerId = passengerId
                };

                return _passengerFlightRepository.Add(passengerFlight);

            }
            else
            {
                Error error;
                if (passanger.Result.Count == 0 || flights.Result.Count == 0)
                {
                    error = new Error()
                    {
                        Code = ErrorCode.NotFound,
                        Message = $"No passengers or flights were found - flightId: {flightId} | passengerId: {passengerId}"
                    };
                }
                else
                {
                    error = new Error()
                    {
                        Code = ErrorCode.InternalError,
                        Message = $"More than one passengers or flights were found - flightId: {flightId} | passengerId: {passengerId}"
                    };
                }

                return new OperationResult<PassengerFlight>(error);
            }
           
        }

    }
}
