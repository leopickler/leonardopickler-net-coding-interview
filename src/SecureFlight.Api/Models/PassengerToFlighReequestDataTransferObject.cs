﻿namespace SecureFlight.Api.Models
{
    public class PassengerToFlighReequestDataTransferObject
    {
        public string PassengerId { get; set; }      

        public long FlightId { get; set; }
        
    }
}
