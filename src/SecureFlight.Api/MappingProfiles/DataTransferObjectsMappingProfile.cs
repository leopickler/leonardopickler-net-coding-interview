using AutoMapper;
using SecureFlight.Api.Models;
using SecureFlight.Core.Entities;

namespace SecureFlight.Api.MappingProfiles;

public class DataTransferObjectsMappingProfile : Profile
{
    public DataTransferObjectsMappingProfile()
    {
        CreateMap<Airport, AirportDataTransferObject>();
        CreateMap<Flight, FlightDataTransferObject>();
        CreateMap<Passenger, PassengerDataTransferObject>();
        CreateMap<PassengerFlight, PassengerToFlighResponseDataTransferObject>()
            .ForMember(dest => dest.PassengerName, options => options.MapFrom(src => string.Format("{0} {1}", src.Passenger.FirstName, src.Passenger.LastName)))
            .ForMember(dest => dest.DestinationAirport, options => options.MapFrom(src => src.Flight.DestinationAirport))
            .ForMember(dest => dest.OriginAirport, options => options.MapFrom(src => src.Flight.OriginAirport))
            .ForMember(dest => dest.PassengerEmail, options => options.MapFrom(src => src.Passenger.Email))
            .ForMember(dest => dest.Code, options => options.MapFrom(src => src.Flight.Code));
    }
}